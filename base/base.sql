CREATE ROLE project LOGIN password '1234';
create database restaurant;
alter database restaurant owner to project;

Create table Profil(
		idprofil serial Primary key,
		name Varchar(20)
);

Create table Employee (
		idEmployee serial Primary key,
		idprofil Varchar(10),
		Name Varchar(20),
		password Varchar(50),
		jourDisponibilite Varchar(50)
);

Create table CategoriePlat(
		idCategorie serial Primary key,
		nom Varchar(40)
);


Create table Plat(
		idPlat serial Primary key,
		idCategorie Varchar(10),
		nom Varchar(20),
		Prix Money,
		Description Varchar(40),
		jourDisponibilite int
);

Create table Payement(
		Idpayement serial Primary key,
		idcorps Varchar(10),
		Valeur Money,
		Foreign key(idcorps) References Corps(idcorps)
); 

Create table Corps(
		Idcorps serial Primary key,
		Nom Varchar(20)
);

Create table commande(
		idcommande serial Primary key,
		idTable varchar(50),
		idClient varchar(50),
		idPlat Varchar(20),
		jourCommande Date
);

Create table Place(
		idTable serial Primary key,
		idEmployee varchar(10)
);


insert into Profil values(default,'Comptable');
insert into Profil values(default,'Serveur');

insert into Employee values(default,'2','Jade','Jade','Lundi');
insert into Employee values(default,'2','Sophie','Sophie','Mardi');
insert into Employee values(default,'1','Mickael','Mickael','Lundi');
insert into Employee values(default,'1','Andry','Andry','Mercredi');
insert into Employee values(default,'2','Gary','Gary','Vendredi');
insert into Employee values(default,'2','Fred','Fred','Mercredi');
insert into Employee values(default,'2','Jude','Jude','Lundi');
insert into Employee values(default,'2','Sarah','Sarah','Jeudi');

insert into CategoriePlat values (default,'Soupe');
insert into CategoriePlat values (default,'Patte');
insert into CategoriePlat values (default,'Petit dejeuner');
insert into CategoriePlat values (default,'Dessert');

insert into Corps values (default,'Espece');
insert into Corps values (default,'Carte');

insert into Plat values(default,'1','Mine Sao',15.5,'Le mine sao est un plat vietnamien',1);
	
insert into Plat values(default,'2','Macaroni',5,'Les macaronis sont une variété de pâtes alimentaires',3);
insert into Plat values(default,'2','Salades de pattes',6,'des patte compose melange  salades',6);
insert into Plat values(default,'3','Pain au raissin',3,'également appelé brioche au raissin',4);
insert into Plat values(default,'3','Pain au Croissant',2.5,'Un croissant est une viennoiserie',7);

insert into Plat values(default,'4','Flan',1.4,'Un flan pâtissier est un mets popularisé',3);
insert into Plat values(default,'4','Yaourt',2,'Un lait fermenté par le développement',5);

insert into Place values(default,'1');
insert into Place values(default,'2');
insert into Place values(default,'5');
insert into Place values(default,'6');
insert into Place values(default,'7');
insert into Place values(default,'8');