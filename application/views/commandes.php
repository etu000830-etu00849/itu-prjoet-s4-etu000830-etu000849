<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Restaurant Kusina</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lovers+Quarrel" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/animate.css">
    
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/aos.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ionicons.min.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
  </head>
<body>
    <div>
    
    <?php for($i<0;$i<count($liste);$i++){?>
    <div class="container">
     <div class="row" align="center">
        <span id="subtitile" ><caption class="text-center" ><h1>Table <?php echo ($i+1);?></h1></caption></span>
     </div>
     <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th><i>Pizza Name</i></th>
                    <th><i>Model</i></th>
                    <th><i>Ingredients</i></th>
                    <th><i>Number</i></th>
                    <th><i>Price(piece/Ar)</i></th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
            </tfoot>
            <tbody>
            <?php for($j=0;$j<$liste[$i];$j++){ ?>
                <tr>
                    <td><?php echo $liste->jourDeCommande;?></td>
                </tr>
            <?php } ?>
            
            </tbody>

        </table>
    </div>
            <?php } ?>
            
    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.stellar.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/aos.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.animateNumber.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.timepicker.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/scrollax.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/google-map.js"></script>
    <script src="<?php echo base_url()?>assets/js/main.js"></script>
        
</body>
</html>