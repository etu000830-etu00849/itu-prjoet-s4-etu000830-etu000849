<body>
    <div>
    <div class="container">
  
    </div>
    <div class="container">
     <div class="row" align="center">
        <span id="subtitile" ><caption class="text-center" ><h1>Your Orders</h1></caption></span>
     </div>
   
     <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th><i>Pizza Name</i></th>
                    <th><i>Model</i></th>
                    <th><i>Ingredients</i></th>
                    <th><i>Number</i></th>
                    <th><i>Price(piece/Ar)</i></th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
            </tfoot>
            <tbody>
            <form action = "ordering.jsp" method="post" >
            
                <tr>
                    <td><img src="pictures/<% out.println(orderImage[i]+".jpg");%>" width="50" height="50"></td>
                    <td><% out.println(((Pizza)listOrder.get(i)).getName()); %></td>
                    <td><% out.println(((Pizza)listOrder.get(i)).getModel()); %></td>
                    <td><% out.println(((Pizza)listOrder.get(i)).getIngredients()); %></td>
                    <td><input type="number" name="number" min="1" max="10" value="<%=numberOrder.get(i)%>"></td>
                    <td><% out.println(((Pizza)listOrder.get(i)).getPrice()); %> Ar</td>
                    <td><button name="buy" type="submit" class="btn btn-sm btn-warning" value="<%out.print(i);%>">Buy</button></td>
                    <td><button name="delete" type="submit" class="btn btn-sm btn-danger" value="<%out.print(i);%>">Delete</button></td>
                </tr>
            
            </form>
            </tbody>

        </table>
    </div>
</body>
</html>