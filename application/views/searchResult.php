<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Restaurant Fanilo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lovers+Quarrel" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/animate.css">
    
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/aos.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ionicons.min.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
  </head>
  <body>
<section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-7 text-center heading-section ftco-animate">
          	<span class="subheading">Specialties</span>
            <h2 class="mb-4">Our Menu</h2>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-lg-4 menu-wrap">
        		<div class="heading-menu text-center ftco-animate">
        			<h3>Produit</h3>
        		</div>
        <?php for($i=0;$i<count($search);$i++){?>
        		<div class="menus d-flex ftco-animate">
              <div class="menu-img img" style="background-image: url(<?php echo base_url()?>assets/images/breakfast-1.jpg);"></div>
              <div class="text">
              	<div class="d-flex">
	                <div class="one-half">
	                  <h3><?php echo $search[$i]->nom;?></h3>
	                </div>
	                <div class="one-forth">
	                  <span class="price"><?php echo $search[$i]->prix;?></span>
	                </div>
	              </div>
	              <p><span><?php echo $search[$i]->description;?></span></p>
                  <p><a href="<?php echo base_url();?>index.php/Commande/insertPanier" class="btn btn-primary px-4 py-3 mt-3">Commander</a></p>
              </div>
            </div>
        <?php } ?>
           
        	    </div>
            </div>
		</section>
            <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-migrate-3.0.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.stellar.min.js"></script>
<script src="<?php echo base_url()?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url()?>assets/js/aos.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.animateNumber.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.timepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/js/scrollax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/google-map.js"></script>
<script src="<?php echo base_url()?>assets/js/main.js"></script>
  
</body>
</html>