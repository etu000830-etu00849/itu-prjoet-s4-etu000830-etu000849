<?php 
   Class Commandes extends CI_Model { 
	
      Public function __construct() { 
         parent::__construct(); 
      }
      
      Public function getAllCommandesFromBase(){
            $result = $this->db->query("select * from commande");
            return $result->result();
      }


      public function makingCommandeByTable($commandes,$tables){
        $commandeByTable = array();    
        for($i=0;$i<count($tables);$i++){
            for($j=0;$j<count($commandes);$j++){
                if($commandes[$j]->idTable==$table[$i]->idTable){
                    $commandeByTable.push($commandes[$j]);
                }
            }
        }

        return $commandeByTable;
    }

      public function getCommandeByTable(){
            $commandes = $this->getAllCommandesFromBase();

            $this->load->model('Place');
            $tables = $this->Place->getAllPlacesFromBase();  
            
            $commandeByTable = $this->makingCommandeByTable($commandes,$tables);
            return $commandeByTable;
      }

     
   } 
?>