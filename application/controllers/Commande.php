<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Commande extends CI_Controller {
    Public function __construct() { 
        parent::__construct(); 
     }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        //$this->load->database();
        //$this->load->model('Profil');
		//	On lance une requête
		//$data['profil'] = $this->Profil->getAllProfilFromBase();

		//	On inclut une vue
	    $this->load->view('acceuil');
	}

	public function insertPanier(){
		$this->load->model('Plat');
		//$this->load->library('cart');
		$search = $this->input->post("idPlat");  
		$data =$this->Plat->searchByName($search);
		$this->cart->insert($data);
		$this->load->view('index');
	}

	public function getCommandByPlace(){
		$sql = "select * from Commande join place on Commande.idTable =(cast(place.idTable as varchar))";
	}

	public function commandeByTable(){
		$this->load->model('Commandes');
		$data['liste'] = $this->Commandes->getCommandeByTable();
		$this->load-view('commandes',$data);
	}
}
