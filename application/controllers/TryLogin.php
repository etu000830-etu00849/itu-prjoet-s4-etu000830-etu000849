<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TryLogin extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
    }
    
    public function authentification(){
        $user = $this->input->post("userName");  
        $pass = $this->input->post("password");  

        $this->load->model('Employee');
        $result = $this->Employee->getEmployeeByName($user);

        $counting = count($result);
        if($counting>0){
            if($pass == $result[0]->password){
                if($result[0]->idprofil=="1"){
                    $this->load->view('dashBoard');
                }
                else{
                    $this->load->view('serveur');
                }
                
            }
            else{
                $this->load->view('login');
            }
        }
        else{
            $this->load->view('login');
        }


        
    }
}
