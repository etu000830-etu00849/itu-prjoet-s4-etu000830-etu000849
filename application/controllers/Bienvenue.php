<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bienvenue extends CI_Controller {
    Public function __construct() { 
        parent::__construct(); 
     }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        //$this->load->database();
        //$this->load->model('Profil');
		//	On lance une requête
		//$data['profil'] = $this->Profil->getAllProfilFromBase();

		//	On inclut une vue
	    $this->load->view('acceuil');
	}
	public function menu()
	{
		$this->load->database();
		$this->load->model('Plat');
		$data['soupe']=$this->Plat->getAllPlatSoupe();
		$data['patte']=$this->Plat->getAllPlatPatte();
		$data['Dessert']=$this->Plat->getAllPlatDessert();
		$this->load->view('menu',$data);
	}
	public function test($a)
	{
		$data['id']=$a;
		$this->load->database();
		$this->load->model('Plat');
		$data['kaly']=$this->Plat->Rechercheprecise($a);
		
		$this->load->view('index',$data);
	}
	public function create()
	{
		$this->load->database();
		$this->load->model('Plat');
		$data['kaly']=$this->Plat->getpl();	
		$this->load->view('create',$data);
	}

}
